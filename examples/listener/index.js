const amqp = require('amqplib');

const consumeFromRabbitMQ = async (topic) => {
  const connection = await amqp.connect('amqp://user:secret@10.79.0.10');
  const channel = await connection.createChannel();

  channel.assertExchange(topic, 'topic', {
    durable: true
  });

  const q = await channel.assertQueue('', {
    exclusive: true
  });

  console.log(`Waiting for messages in topic: ${topic}. To exit press CTRL+C`);

  channel.bindQueue(q.queue, topic, '');

  channel.consume(q.queue, (msg) => {
    if (msg.content) {
      console.log(`Received: ${msg.content.toString()} from topic: ${topic}`);
    }
  }, {
    noAck: true
  });
};

const topics = ['general', 'messages', 'logs', 'transactions'];

topics.forEach(topic => {
  consumeFromRabbitMQ(topic);
});
