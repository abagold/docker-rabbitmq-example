const amqp = require('amqplib');
const { faker } = require('@faker-js/faker');

const sendToRabbitMQ = async (topic, message) => {
  const connection = await amqp.connect('amqp://user:secret@10.79.0.10');
  const channel = await connection.createChannel();

  channel.assertExchange(topic, 'topic', {
    durable: true
  });

  channel.publish(topic, '', Buffer.from(JSON.stringify(message)));
  console.log(`Sent ${JSON.stringify(message)} to topic ${topic}`);

  setTimeout(() => {
    connection.close();
  }, 500);
};

const topics = ['general', 'messages', 'logs', 'transactions'];

setInterval(() => {
  const message = {
    id: faker.string.uuid(),
    timestamp: Date.now(),
    amount: faker.finance.amount(),
    description: faker.lorem.sentence()
  };

  topics.forEach(topic => {
    sendToRabbitMQ(topic, message);
  });
}, 1);