# Docker example for RabbitMQ

Run the service with 

```bash

docker-compose up
```

you can test using nodejs

```bash
# listener
cd ./examples/listener
npm i
node index

# sender
cd ./examples/sender
npm i
node index


```