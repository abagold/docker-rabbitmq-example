const crypto = require('crypto');
const password = process.argv[2];

// 1. Generate a random 32 bit salt:
const salt = crypto.randomBytes(4);

// 2. Concatenate that with the UTF-8 representation of the plaintext password
const tmp0 = Buffer.concat([salt, Buffer.from(password)]);

// 3. Take the SHA256 hash and get the bytes back
const tmp1 = crypto.createHash('sha256').update(tmp0).digest();

// 4. Concatenate the salt again:
const saltedHash = Buffer.concat([salt, tmp1]);

// 5. Convert to base64 encoding:
const passHash = saltedHash.toString('base64');

console.log(passHash);
